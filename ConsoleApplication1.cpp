﻿#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <time.h>
#include <Windows.h>
#include <vector>
#include "nnunet.h"


using namespace std;


int main(void)
{
	time_t start_time;
	time_t end_time;

	time(&start_time);	
	double seconds;

	// 输入必须以   ../148/148_0000.nii.gz   的格式输入其中148文件夹下下仅有一个nii
	// TODO:
	string input = "d:\\work\\project\\kidney\\nnunet\\test\\cta\\wd0179_0000.nii.gz";
	string output = "d:\\work\\project\\kidney\\nnunet\\test\\prediction";
	cout << "begin" << endl;

	Nnunet* nnunet = new Nnunet(input, output, "500", "fast");
	// maybe we know the output path

	// mkdir predict folder in output path
    

	// prediction
	nnunet->comment();
	//int test = system("echo 1 &");
	seconds = difftime(time(&end_time), start_time);

	// remove copy nii.gz and folder
	//finish
	cout << "it spent " << seconds << "s"<<endl;
}
