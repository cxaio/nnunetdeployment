#ifndef BIRD_H
#define BIRD_H

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <time.h>
#include <Windows.h>
#include <vector>
#include "nnunet.h"
#include <numeric>

using namespace std;

/*
* 方案一： 运行命令行
* 方案二：运行exe文件(nnUNet环境不受影响)
* 问题：
	1. nnUNet reference 采用输入文件夹的形式：target/xxx_0001.nii.gz
	2. 预测文件名需为xxx_0001
*/
class Nnunet 
{
	private:
		string target_input;
		string target_output;
		string task_id;
		string target_pattern;
	public:
		Nnunet(string input, string output, string task = "500", string pattern="fast") {
		/*	以命令的形式调用nnUNet框架
		* 	@param input :必须以   ../xxx/xxx_0000.nii.gz   的格式输入其中148文件夹下下仅有一个nii
		* 	@param output:输出路径
		*	@param task ：任务ID，500代表cta，504代表排泄，998代表平扫，默认cta
		*	@param pattern: fast 为快速分割模型，precise为精准模型
		*
		*/
			target_input = input;
			target_output = output;
			task_id = task;
			target_pattern = pattern;
		};
		~Nnunet() {
		};

	public:
		string get_nifit_folder() {
			/*
				get the nii parent folder path
			*/
			char* s = const_cast<char*>(target_input.data());
			char* p;
			vector<string> result;
			string output;
			const char* delim = "\\";
			p = strtok(s, delim);

			while (p) {
				string temp = p;
				result.push_back(temp + "\\");
				p = strtok(NULL, delim);
			}
			output = accumulate(result.begin(), result.end() - 1, output);
			//cout << output << endl;
			return output;
		}

	public:
		int comment() {

			string input = get_nifit_folder();
			//nnUNet_predict - i INPUT_FOLDER - o OUTPUT_FOLDER - t Task501_xxx -m 3d_cascade_fullres
			//nnUNet_predict - i D : \work\project\kidney\nnunet\test\cta\ - o D : \work\project\kidney\nnunet\test\cta\ - t 500 - m 3d_cascade_fullres - f 4
			string pre_comment;
			if (target_pattern == "precise") {
				pre_comment = "nnUNet_predict -i " + input + " -o " + target_output + " -t " + task_id + " -m 3d_cascade_fullres -f 4 --overwrite_existing";
			}
			else {
				pre_comment = "nnUNet_predict -i " + input + " -o " + target_output + " -t " + task_id + " -m 3d_cascade_fullres -f 4 --overwrite_existing --disable_tta";

			}

			cout << pre_comment.data() << std::endl;
			system(pre_comment.data());

			return 0;
		}



};

#endif // BIRD_H
